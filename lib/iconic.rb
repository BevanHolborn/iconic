# frozen_string_literal: true

require 'iconic/configuration'
require 'iconic/asset'
require 'iconic/icon'