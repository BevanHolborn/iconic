module Iconic

  class << self
    def configuration
      @configuration ||= Configuration.new
    end

    def configure
      yield(configuration)
    end
  end

  class Configuration

    attr_accessor :icon_asset_path

    def initialize
      @icon_asset_path = "app/assets/icons"
    end

  end

end