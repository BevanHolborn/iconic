module Iconic
  module Icon

    def icon(identifier)
      icon_path = Asset.icon_asset_path(identifier)
      Nokogiri::XML(File.read(icon_path)).at_css('svg')
    end

  end
end