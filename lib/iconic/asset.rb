module Iconic
  module Asset

    def icon_asset_path(identifier)
      if Rails.env.local?
        Rails.root.join(configuration.icon_asset_path, "#{identifier}.svg")
      else
        File.join(configuration.icon_asset_path, ActionController::Base.helpers.asset_path("#{identifier}.svg"))
      end
    end

  end
end