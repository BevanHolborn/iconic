# frozen_string_literal: true

require_relative "lib/iconic/version"

Gem::Specification.new do |spec|
  spec.name = "iconic"
  spec.version = Iconic::VERSION
  spec.authors = ["Bevan Holborn"]
  spec.summary = "Write a short summary, because RubyGems requires one."
  spec.files = Dir["lib/**/*.rb"] + Dir["bin/*"]

  spec.add_runtime_dependency 'nokogiri', '>= 1.16'
end
